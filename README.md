# Scrappy

Script to send out alerts on price changes for select items on the Steam Market.

## Prerequisites

This script uses requests, pushbullet.py and python-magic modules.

## Description

In any individual page of a Steam Market item, the data corresponding to the quantity
and price of buy/sell orders is injected and then updated using an AJAX request which
returns a JSON object with the data.

So, to get the relevant data from a regular market item link, the script needs to find
the item_nameid to then be able to make a request for the JSON object from which it
takes the number of items being sold and at what price. From there it just compares it
to the desired price and if its greater sends an alert.

## Usage

To set up the alerts, you'll need your own personal Access Token, which you can get
from your Pushbullet account, written to a file named "pushbullet_token".

Write the Steam market items links and the price for the alert separated by a comma,
to the "market_links.csv" file, one link and price per line, and the script will do
the rest.

## Deployment

The script could be deployed to a raspberry pi and used to monitor whichever item 
with a cron job.

## Built With

* [Python 3.x](https://docs.python.org/3/)

## Author

* **Antonio Ponzano** - [ajponzano](https://gitlab.com/ajponzano)