#!/usr/bin/env python3
# 03/05/18 - By Aj
#
# A script used to monitor selected Steam market items and send an alert upon
# specified criteria is met.
#

import argparse
import market_scraper
import os


# Argument checks
parser = argparse.ArgumentParser(description="Script used to monitor " +
                                             "selected Steam market " +
                                             "items and send an alert " +
                                             "upon specified criteria " +
                                             "is met.")
parser.add_argument("-d", "--dryrun", action="store_true",
                    help="run the script without scraping any site")
parser.add_argument("-l", "--logging", action="store_true",
                    help="log output to a file")
parser.add_argument("-v", "--verbose", action="store_true",
                    help="increase output verbosity")
args = parser.parse_args()

# Arguments logic check
if args.verbose and not args.logging:
    # print("Verbose %s\nLogging %s" % (args.verbose, args.logging))
    def verboseprint(*args, **kwargs):
        print(*args, **kwargs)
elif args.logging and not args.verbose:
    # print("Verbose %s\nLogging %s" % (args.verbose, args.logging))
    def verboseprint(*args, **kwargs):
        pass
elif args.verbose and args.logging:
    # print("Verbose %s\nLogging %s" % (args.verbose, args.logging))
    def verboseprint(*args, **kwargs):
        print(*args, **kwargs)
else:
    # print("Verbose %s\nLogging %s" % (args.verbose, args.logging))
    def verboseprint(*args, **kwargs):
        pass

# Object instantiation
scraper = market_scraper.SteamMarketScraper(args.dryrun, verboseprint)
pb = market_scraper.safe_pushbullet_instantiation(verboseprint)
object_holder = []

# Object instantiation but only for those on the watchlist
for item in scraper.market_items:
    if item['watch'] != 0:
        object_holder.append(market_scraper.SteamMarketItem(item,
                                                            verboseprint))

# Check if object list is empty and print data
if not object_holder:
    print("\nNo links on market_links file. Nothing to do here.")
else:
    for obj in object_holder:
        obj.check_item_price(args.dryrun, pb)

print()  # For readability
