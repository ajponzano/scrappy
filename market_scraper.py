# 07/05/18 - By Aj

import csv
import json
import os
import re
import requests
import time

from pushbullet import Pushbullet


def safe_pushbullet_instantiation(verboseprint):
    """
    Checks for a Pushbullet token, makes a Pushbullet object and if everything
    goes well, returns that object.
    """
    try:
        with open('pushbullet_token', newline='') as f:
            if os.stat('pushbullet_token').st_size != 0:
                token = f.read()
                verboseprint("\nPushbullet token loaded.")
            else:
                verboseprint("\nEmpty token file.")
    except Exception as e:
        print("\npushbullet_token loading failed. " +
              "There was a problem: %s" % (e))

    try:
        pb = Pushbullet(token)
    except Exception as e:
        print("\nProblem with Pushbullet: %s" % (e))
        pb = []

    return pb


class SteamMarketScraper:

    market_items = []
    market_links = []

    def __init__(self, dryrun, func, *args):
        self.verboseprint = func

        self.load_data()

        if os.path.exists('market_items.json'):
            templist = self.check_if_scraped(self.market_links)
            self.scrape_links(templist, dryrun)
        else:
            self.scrape_links(self.market_links)

        self.set_items_to_watch(self.market_links)
        self.save_links_data(dryrun)

    def load_data(self):
        """
        Load market items info and links.
        """
        try:
            with open('market_links.csv', newline='') as f:
                if os.stat('market_links.csv').st_size != 0:
                    reader = csv.reader(f)
                    self.market_links = list(map(tuple, reader))
                    self.verboseprint("market_links.csv file loaded.")
                else:
                    self.verboseprint("market_links.csv file is empty. " +
                                      "Nothing to load.")
        except Exception as e:
            print("market_links.csv loading failed. " +
                  "There was a problem: %s" % (e))

        try:
            with open('market_items.json') as json_data:
                if os.stat('market_items.json').st_size != 0:
                    self.market_items = json.load(json_data)
                    self.verboseprint("market_items.json file loaded.")
                else:
                    self.verboseprint("market_items.json file is empty. " +
                                      "Nothing to load.")
        except Exception as e:
            print("market_items.json loading failed. " +
                  "There was a problem: %s" % (e))

    def check_if_scraped(self, linkslist):
        """
        Check if every link in the list has had its data scraped.
        Returns a list containing links to be scraped.
        """
        scraped = False
        newlist = []

        self.verboseprint("\nChecking links...")

        for link in linkslist:
            for item in self.market_items:
                if link[0] == item['link']:
                    scraped = True

                    self.verboseprint("\'%s\' already scraped." %
                                      (item['name']))

            if not scraped:
                newlist.append(link)

                self.verboseprint("\'%s\' not scraped." % (link[0]))

            scraped = False  # Flag reset

        return newlist

    def set_items_to_watch(self, linkslist):
        """
        Sets all the items corresponding to the original links list
        to be checked for price or not and also updates the price alert.
        """
        not_on_list = True

        self.verboseprint("\nChecking watchlist...")

        for item in self.market_items:
            for link in linkslist:
                if item['link'] == link[0]:
                    item['watch'] = 1   # Item to be watched
                    item['price_alert'] = float(link[1])  # Price update
                    not_on_list = False

                    self.verboseprint("\'%s\' on watchlist." % (item['name']))

            if not_on_list:
                item['watch'] = 0   # Item no longer watched

                self.verboseprint("\'%s\' NOT on watchlist." % (item['name']))

            not_on_list = True  # Flag reset

    def scrape_links(self, linkslist, dryrun):
        """
        Gets every name and itemid of the links list and saves it.
        """
        if not not linkslist:   # If the list is not empty
            i = 1

            print()  # For readability

            for link in linkslist:
                namelist = self.get_item_info(link[0], dryrun)

                if not dryrun:
                    item_info = {
                                'link': link[0],
                                'name': namelist[0],
                                'item_nameid': namelist[1],
                                'price_alert': float(link[1]),
                                'watch': 1
                                }

                    self.market_items.append(item_info)
                    print("Scraping: %s of %s - %s" % (i, len(linkslist),
                                                       namelist[0]))
                    i += 1
                    time.sleep(2)
                else:
                    print("Not scraping: %s of %s - %s" % (i, len(linkslist),
                                                           link[0]))
                    i += 1

    def get_item_info(self, marketlink, dryrun):
        """
        From a regular market item link, gets the name and item_nameid.
        Returns a list with both.
        """
        namelist = []

        if not dryrun:
            htmlsource = requests.get(marketlink)

            try:
                htmlsource.raise_for_status()
                htmlsource = htmlsource.text
            except Exception as e:
                print("There was a problem: %s" % (e))

            # Get the name
            name = re.search(r'for.+?<', htmlsource).group()
            namelist.append(name[4:-1])
            # Get the id
            nameid = re.search(r'art\(.\d*', htmlsource).group()
            namelist.append(re.search(r'\d.*', nameid).group())

        return namelist

    def save_links_data(self, dryrun):
        if not dryrun:
            with open('market_items.json', 'w') as json_data:
                json.dump(self.market_items, json_data, indent=4)
        else:
            self.verboseprint("\nData not saved.")


class SteamMarketItem:

    def __init__(self, item, func, *args):
        self.verboseprint = func
        self.item_nameid = item['item_nameid']
        self.name = item['name']
        self.price_alert = item['price_alert']
        self.market_link = item['link']
        self.json_link = self.construct_item_data_request(self.item_nameid)

    def get_item_price(self, dryrun):
        """
        Returns a list with an int and a float, quantity and price.
        """
        if not dryrun:
            res = requests.get(self.json_link)

            try:
                res.raise_for_status()
                summary = res.json()
            except Exception as exc:
                print("There was a problem: %s" % (exc))

            summary = summary["sell_order_summary"]

            datalist = re.findall(r'\d.*?<', summary)
            datalist = self.strip_last_char(datalist)

            time.sleep(1)
        else:
            datalist = ['0', '0']

        self.verboseprint("\n\t%s" % (self.name))
        self.verboseprint("\t%s for sale starting at ARS$ %s" % (datalist[0],
                                                                 datalist[1]))

        # String conversion
        datalist.append(int(datalist.pop(0)))
        datalist.append(float((datalist.pop(0)).replace(',', '.')))

        return datalist

    def check_item_price(self, dryrun, pb):
        """
        Gets item price data, checks it a against the price alert and sends
        an alert.
        """
        data = self.get_item_price(dryrun)

        if data[1] >= self.price_alert:
            push = pb.push_note("Scrappy price alert!!!",
                                "{0} is above ARS$ ".format(self.name) +
                                "{0} - Current".format(str(self.price_alert)) +
                                " price is ARS$ {0}.".format(str(data[1])))
            push = pb.push_link("Market page".format(self.name), 
                                self.market_link)
            self.verboseprint("\nPushbullet alert sent! Message: \n\n\t" +
                              "{0} is above ARS$ ".format(self.name) +
                              "{0} - Current ".format(str(self.price_alert)) +
                              "price is ARS$ {0}".format(str(data[1])))

    def strip_last_char(self, lst):
        """
        Strips the last character from each item in the list and
        returns it
        """
        i = 0

        while i < len(lst):
            lst[i] = lst[i][:-1]
            i += 1

        return lst

    def construct_item_data_request(self, nameid):
        link = ("https://steamcommunity.com/market/itemordershistogram?" +
                "language=english&" + "currency=34&" +
                "item_nameid=" + nameid)
        return link
